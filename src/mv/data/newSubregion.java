/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author fang
 */
public class newSubregion {
    ObservableList<newPolyList> subregion;
    int size;
    
    public newSubregion(){
        subregion = FXCollections.observableArrayList();
        size=0;
    }
    
    public void addPolyList(newPolyList polyList){
        subregion.add(polyList);
    }
    public void setSize(int initSize){
        size=initSize;
    }
    public int getSize(){
        return size;
    }
    public int getPoint(int index){
        return subregion.get(index).getSize();
    }
    public double getX(int i,int j){
        return subregion.get(i).getX(j);
    }
    public double getY(int i,int j){
        return subregion.get(i).getY(j);
    }
    public void setX(int i,int j,double x){
        subregion.get(i).setX(j,x);
    }
    public void setY(int i,int j,double y){
        subregion.get(i).setY(j,y);
    }
    /*public ObservableList<Double> getPointList(int i){
        return subregion.get(i).getPointsList();
    }*/
    
    
    
}
