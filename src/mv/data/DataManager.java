package mv.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import saf.components.AppDataComponent;
import mv.MapViewerApp;


/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    ObservableList<newSubregion> subregionList;
    ObservableList<newPolyList> longitudeList;
    ObservableList<newPolyList> latitudeList;
    int size;
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        size=0;
        subregionList=FXCollections.observableArrayList();
        longitudeList=FXCollections.observableArrayList();
        latitudeList=FXCollections.observableArrayList();
    }
    public void loadLong(){
        for(int i=0;i<13;i++){
            newPolyList longGrid=new newPolyList();
            newPoly longPoints=new newPoly(i*30,0.0);
            longGrid.AddPoly(longPoints);
            longPoints=new newPoly(i*30,180.0);
            longGrid.AddPoly(longPoints);
            longitudeList.add(longGrid);
        }
           
        for(int i=0;i<7;i++){
            newPolyList longGrid=new newPolyList();
            newPoly longPoints=new newPoly(0.0,i*30);
            longGrid.AddPoly(longPoints);
            longPoints=new newPoly(360.0,i*30);
            longGrid.AddPoly(longPoints);
            latitudeList.add(longGrid);
        }
        
    }
    public Double getGridX(int i,int j){
        return longitudeList.get(i).getX(j);
    }
    public Double getGridY(int i,int j){
        return longitudeList.get(i).getY(j);
    }
    public Double getlatX(int i,int j){
        return latitudeList.get(i).getX(j);
    }
    public Double getlatY(int i,int j){
        return latitudeList.get(i).getY(j);
    }
    public void setGridX(int i, int j, double value){
        longitudeList.get(i).setX(j, value);
    }
    public void setGridY(int i, int j, double value){
        longitudeList.get(i).setY(j, value);
    }
    public void setlatX(int i, int j, double value){
        latitudeList.get(i).setX(j, value);
    }
    public void setlatY(int i, int j, double value){
        latitudeList.get(i).setY(j, value);
    }
    public void add(newSubregion subregion){
        subregionList.add(subregion);
        
        
    }
    public void setSize(int initSize){
        size=initSize;
    }
    public int getSize(){
        
        return size;
    }
    public int getPolySize(int index){
        return subregionList.get(index).getSize();
    }
    public int getPointSize(int i,int j){
        return subregionList.get(i).getPoint(j);
    }
    /*public ObservableList<Double> getPoints(int i,int j){
        return subregionList.get(i).getPointList(j);
    }*/
    public double getX(int i,int j, int k){
        return subregionList.get(i).getX(j,k);
    }
    public void setX(int i,int j, int k, double x){
        subregionList.get(i).setX(j,k,x);
        
    }
    public void setY(int i,int j, int k, double y){
        subregionList.get(i).setY(j,k,y);
        
    }
    public double getY(int i,int j, int k){
        return subregionList.get(i).getY(j,k);
    }
    @Override
    public void reset() {
        subregionList.clear();
    }
}
