/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

/**
 *
 * @author fang
 */
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author fang
 */
public class newPoly {
    public static final double DEFAULT_X=0;
    public static final double DEFAULT_Y=0;
    
    final DoubleProperty x;
    final DoubleProperty y;
    
    ObservableList<Double> points;
    
    public newPoly(){
        x = new SimpleDoubleProperty(DEFAULT_X);
        y= new SimpleDoubleProperty(DEFAULT_Y);
        points=FXCollections.observableArrayList();
    }
    
    public newPoly(double initX, double initY){
        this();
        x.set(initX);
        y.set(initY);
        points=FXCollections.observableArrayList();
    }
    
    public double getX(){
        return x.get();
    }
    
    public void setX(double value){
        x.set(value);
    }
    
    public DoubleProperty xProperty(){
        return x;
    }
    
    public double getY(){
        return y.get();
    }
    
    public void setY(double value){
        y.set(value);
    }
    
    public DoubleProperty yProperty(){
        return y;
    }
    public void AddPoints(double x,double y){
        points.addAll(x,y);
    }
    
    public void reset(){
        setX(DEFAULT_X);
        setY(DEFAULT_Y);
    }
    
}
