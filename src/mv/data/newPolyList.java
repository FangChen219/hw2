/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author fang
 */
public class newPolyList {
    ObservableList<newPoly> newPolyList;
    
    
    public newPolyList(){
         newPolyList = FXCollections.observableArrayList();
        
    }
    
    public void AddPoly(newPoly poly){
        newPolyList.addAll(poly);
    }
    public int getSize(){
        return newPolyList.size();
    }
    public double getX(int i){
        return newPolyList.get(i).getX();
    }
    public double getY(int i){
        return newPolyList.get(i).getY();
    }
    public void setX(int i,double x){
        newPolyList.get(i).setX(x);
    }
    public void setY(int i,double y){
        newPolyList.get(i).setY(y);
    }
    public ObservableList<newPoly> getList(){
        return newPolyList;
    }
    /*public ObservableList<Double> getPointsList(){
        return newPolyList;
    }*/
}
