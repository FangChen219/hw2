/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.shape.Polygon;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polyline;
import javafx.stage.Stage;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.controller.MapViewController;
import mv.data.DataManager;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    MapViewController mapViewController;
    Button btn1;
    Pane grid;
    int numberOfPolygon;
    int numberOfSubregion;
    int numberOfPoints;
    double paneHight;
    double paneWidth;
    double centerX;
    double centerY;
    boolean toggle;
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        workspace = new Pane();
        workspace.setStyle("-fx-background-color: #0000ff;");
        app.getGUI().setFileToolBar(app);
        toggle=false;
        //pane=new Pane();
        //app.getGUI().initChildButton(workspace, CLASS_FILE_BUTTON, CLASS_FILE_BUTTON, workspaceActivated)
        //btn1.setText("btn");
        //workspace.getChildren().add(btn1);
        
        
    }
    
    private void loadMap(){
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        numberOfSubregion=dataManager.getSize();
        paneHight=app.getGUI().getPrimaryScene().getHeight();
        paneWidth=app.getGUI().getPrimaryScene().getWidth();
        //paneHight=app.getGUI().getAppPane().getHeight();
        //paneWidth=app.getGUI().getAppPane().getWidth();
        //paneHight=workspace.getHeight();
        //paneWidth=workspace.getWidth();
        centerX=paneWidth/2;
        centerY=paneHight/2;
        for(int i=0;i<numberOfSubregion;i++){
            numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                        polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
              
                workspace.getChildren().addAll(polygon);
            }
            //workspace.getChildren().add(pane);
        }
        
        //workspace.getChildren().add(pane);
        loadGrid();
        //pane.getChildren().add(btn1);
       // workspace.getChildren().add(btn1);
       
    }
    
    public void loadGrid(){
        DataManager dataManager = (DataManager)app.getDataComponent();
        dataManager.loadLong();
        grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);

                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                          if(y<0){
                            y=0;
                        }
                        polyline.getPoints().addAll(x,y);
            }
            if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            //workspace.getChildren().add(pane);
        }
       
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);

                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                          if(y<0){
                            y=0;
                        }
                        polyline.getPoints().addAll(x,y);
            }
            if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
       grid.setVisible(toggle);
        workspace.getChildren().add(grid);
    }
    
    
    public void zoomIn(double mouseX,double mouseY){
        workspace.getChildren().clear();
        double moveX=(centerX-mouseX)*360/paneWidth;
        double moveY=(centerY-mouseY)*180/paneHight;
        DataManager dataManager = (DataManager)app.getDataComponent();
        for(int i=0;i<numberOfSubregion;i++){
          numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        x=x+moveX;
                        y=y+moveY;
                        x=(x-180)*2;//+moveX;
                        y=(90-y)*2;//+moveY;
                        dataManager.setX(i, j, k, x+180);
                        x=dataManager.getX(i, j, k);
                        
                        dataManager.setY(i, j, k, 90-y);
                        y=dataManager.getY(i, j, k);
                        
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                        
                        polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
                workspace.getChildren().addAll(polygon);
            }
      }
        grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);
                x=x+moveX;
                y=y+moveY;
                x=(x-180)*2;//+moveX;
                y=(90-y)*2;//+moveY;
                dataManager.setGridX(i, j, x+180);
                x=dataManager.getGridX(i, j);
                    
                dataManager.setGridY(i, j,  90-y);
                y=dataManager.getGridY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
              if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            
            //workspace.getChildren().add(pane);
        }
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);
                x=x+moveX;
                y=y+moveY;
                x=(x-180)*2;//+moveX;
                y=(90-y)*2;//+moveY;
                dataManager.setlatX(i, j, x+180);
                x=dataManager.getlatX(i, j);
                    
                dataManager.setlatY(i, j,  90-y);
                y=dataManager.getlatY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               //polyline.
          if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
        /*Polyline testpoly = new Polyline();
        grid=new Pane();
        Double[] test=new Double[]{50.0, 0.0,50.0, 100.0,100.0, 0.0 , 100.0, 100.0 };
        testpoly.getPoints().addAll(test);
        grid.getChildren().add(testpoly);*/
        grid.setVisible(toggle);
        workspace.getChildren().add(grid);
        
    }
    public void zoomOut(double mouseX,double mouseY){
         workspace.getChildren().clear();
        double moveX=(centerX-mouseX)*360/paneWidth;
        double moveY=(centerY-mouseY)*180/paneHight;
        DataManager dataManager = (DataManager)app.getDataComponent();
      for(int i=0;i<numberOfSubregion;i++){
          numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        x=x+moveX;
                        y=y+moveY;
                        x=(x-180)/2;//+moveX;
                        y=(90-y)/2;
                        dataManager.setX(i, j, k, x+180);
                        x=dataManager.getX(i, j, k);
                        dataManager.setY(i, j, k, 90-y);
                        y=dataManager.getY(i, j, k);
                        
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                        polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
                workspace.getChildren().addAll(polygon);
            }
      }
       grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);
                x=x+moveX;
                y=y+moveY;
                x=(x-180)/2;//+moveX;
                y=(90-y)/2;
                dataManager.setGridX(i, j, x+180);
                x=dataManager.getGridX(i, j);
                    
                dataManager.setGridY(i, j,  90-y);
                y=dataManager.getGridY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
              if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            
            //workspace.getChildren().add(pane);
        }
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);
                x=x+moveX;
                y=y+moveY;
                x=(x-180)/2;//+moveX;
                y=(90-y)/2;//+moveY;
                dataManager.setlatX(i, j, x+180);
                x=dataManager.getlatX(i, j);
                    
                dataManager.setlatY(i, j,  90-y);
                y=dataManager.getlatY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               //polyline.
           if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
        /*Polyline testpoly = new Polyline();
        grid=new Pane();
        Double[] test=new Double[]{50.0, 0.0,50.0, 100.0,100.0, 0.0 , 100.0, 100.0 };
        testpoly.getPoints().addAll(test);
        grid.getChildren().add(testpoly);*/
        grid.setVisible(toggle);
        workspace.getChildren().add(grid);
    }
    public void setupHandlers(){
        mapViewController = new MapViewController(app);
        
        workspace.setOnMouseClicked(e -> {
            if(e.getButton().equals(e.getButton().PRIMARY))
                mapViewController.zoomIn(e.getScreenX(),e.getY());
            else if(e.getButton().equals(e.getButton().SECONDARY))
                mapViewController.zoomOut(e.getX(),e.getY());
            
        });
        
        app.getGUI().getPrimaryScene().setOnKeyPressed(e -> {
           if(e.getCode().equals(e.getCode().UP)){
               mapViewController.up();
           }
           else if(e.getCode().equals(e.getCode().DOWN)){
               mapViewController.down();
           }
           else if(e.getCode().equals(e.getCode().LEFT)){
               mapViewController.left();
           }
           else if(e.getCode().equals(e.getCode().RIGHT)){
               mapViewController.right();
           }
           else if(e.getCode().equals(e.getCode().G)){
               toggle=!toggle;
               workspace.getChildren().clear();
               loadMap();
               
           }
               
        });
        
        
    }
    
    public void up(){
        workspace.getChildren().clear();
       //double moveX=10*360/paneWidth;
        double moveY=10*180/paneHight;
        DataManager dataManager = (DataManager)app.getDataComponent();
      for(int i=0;i<numberOfSubregion;i++){
          numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        //x=x+moveX;
                        //y=y+moveY;
                        dataManager.setX(i, j, k, x);
                        x=dataManager.getX(i, j, k);
                        
                        dataManager.setY(i, j, k, y-moveY);
                        y=dataManager.getY(i, j, k);
                        
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                            polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
                workspace.getChildren().addAll(polygon);
            }
      }
      grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);
                
              
                dataManager.setGridX(i, j, x);
                x=dataManager.getGridX(i, j);
                    
                dataManager.setGridY(i, j,  y-moveY);
                y=dataManager.getGridY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            
            //workspace.getChildren().add(pane);
        }
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);
               
               
                dataManager.setlatX(i, j, x);
                x=dataManager.getlatX(i, j);
                    
                dataManager.setlatY(i, j,  y-moveY);
                y=dataManager.getlatY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               //polyline.
           if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
        /*Polyline testpoly = new Polyline();
        grid=new Pane();
        Double[] test=new Double[]{50.0, 0.0,50.0, 100.0,100.0, 0.0 , 100.0, 100.0 };
        testpoly.getPoints().addAll(test);
        grid.getChildren().add(testpoly);*/
        grid.setVisible(toggle);
        workspace.getChildren().add(grid);
    }
    public void down(){
        workspace.getChildren().clear();
       //double moveX=10*360/paneWidth;
        double moveY=10*180/paneHight;
        DataManager dataManager = (DataManager)app.getDataComponent();
      for(int i=0;i<numberOfSubregion;i++){
          numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        //x=x+moveX;
                        //y=y+moveY;
                        dataManager.setX(i, j, k, x);
                        x=dataManager.getX(i, j, k);
                        dataManager.setY(i, j, k, y+moveY);
                        y=dataManager.getY(i, j, k);
                        
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                        polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
                workspace.getChildren().addAll(polygon);
            }
      } 
      grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);
                
              
                dataManager.setGridX(i, j, x);
                x=dataManager.getGridX(i, j);
                    
                dataManager.setGridY(i, j,  y+moveY);
                y=dataManager.getGridY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            
            //workspace.getChildren().add(pane);
        }
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);
               
               
                dataManager.setlatX(i, j, x);
                x=dataManager.getlatX(i, j);
                    
                dataManager.setlatY(i, j,  y+moveY);
                y=dataManager.getlatY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               //polyline.
          if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
        /*Polyline testpoly = new Polyline();
        grid=new Pane();
        Double[] test=new Double[]{50.0, 0.0,50.0, 100.0,100.0, 0.0 , 100.0, 100.0 };
        testpoly.getPoints().addAll(test);
        grid.getChildren().add(testpoly);*/
        grid.setVisible(toggle);
        workspace.getChildren().add(grid);
    }
    public void left(){
        workspace.getChildren().clear();
        double moveX=10*360/paneWidth;
        //double moveY=10*180/paneHight;
        DataManager dataManager = (DataManager)app.getDataComponent();
      for(int i=0;i<numberOfSubregion;i++){
          numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        //x=x+moveX;
                        //y=y+moveY;
                        dataManager.setX(i, j, k, x-moveX);
                        x=dataManager.getX(i, j, k);
                        dataManager.setY(i, j, k, y);
                        y=dataManager.getY(i, j, k);
                        
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                        polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
                workspace.getChildren().addAll(polygon);
            }
      }
      grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);
                
              
                dataManager.setGridX(i, j, x-moveX);
                x=dataManager.getGridX(i, j);
                    
                dataManager.setGridY(i, j,  y);
                y=dataManager.getGridY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            
            //workspace.getChildren().add(pane);
        }
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);
               
               
                dataManager.setlatX(i, j, x-moveX);
                x=dataManager.getlatX(i, j);
                    
                dataManager.setlatY(i, j,  y);
                y=dataManager.getlatY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               //polyline.
           if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
        /*Polyline testpoly = new Polyline();
        grid=new Pane();
        Double[] test=new Double[]{50.0, 0.0,50.0, 100.0,100.0, 0.0 , 100.0, 100.0 };
        testpoly.getPoints().addAll(test);
        grid.getChildren().add(testpoly);*/
        grid.setVisible(toggle);
        workspace.getChildren().add(grid);
    }
    public void right(){
        workspace.getChildren().clear();
        double moveX=10*360/paneWidth;
        //double moveY=10*180/paneHight;
        DataManager dataManager = (DataManager)app.getDataComponent();
      for(int i=0;i<numberOfSubregion;i++){
          numberOfPolygon=dataManager.getPolySize(i);
            for(int j=0;j<numberOfPolygon;j++){
                Polygon polygon = new Polygon();
                numberOfPoints=dataManager.getPointSize(i,j);
                //double[] points=new double[numberOfPoints*2];
                //int count=0;
                    for(int k=0;k<numberOfPoints;k++){
                        double x=dataManager.getX(i, j, k);
                        double y=dataManager.getY(i,j,k);
                        //x=x+moveX;
                        //y=y+moveY;
                        dataManager.setX(i, j, k, x+moveX);
                        x=dataManager.getX(i, j, k);
                        dataManager.setY(i, j, k, y);
                        y=dataManager.getY(i, j, k);
                        
                        /*points[count]=x;
                        points[count+1]=y;
                        count=count+2;*/
                        
                        x=x*(paneWidth/360);
                        y=y*(paneHight/180);
                        if(y<0){
                            y=0;
                        }
                        polygon.getPoints().addAll(x,y);
                    }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                polygon.setFill(Color.GREEN);
                polygon.setStroke(Color.BLACK);
                
                workspace.getChildren().addAll(polygon);
            }
      }
      grid=new Pane();
        for(int i=0;i<13;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getGridX(i, j);
                double y=dataManager.getGridY(i,j);
                
              
                dataManager.setGridX(i, j, x+moveX);
                x=dataManager.getGridX(i, j);
                    
                dataManager.setGridY(i, j,  y);
                y=dataManager.getGridY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               if(i==6|i==0|i==12){
                polyline.setStroke(Color.WHITE);
            }
            else{
                polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
            //workspace.getChildren().add(pane);
        }
        for(int i=0;i<7;i++){
            Polyline polyline = new Polyline();
            for(int j=0;j<2;j++){
                double x=dataManager.getlatX(i, j);
                double y=dataManager.getlatY(i,j);
               
               
                dataManager.setlatX(i, j, x+moveX);
                x=dataManager.getlatX(i, j);
                    
                dataManager.setlatY(i, j,  y);
                y=dataManager.getlatY(i, j);
                x=x*(paneWidth/360);
                y=y*(paneHight/180);
                if(y<0){
                            y=0;
                        }
                polyline.getPoints().addAll(x,y);
            }
                
                //polygon.getPoints().addAll(dataManager.getPoints(i,j));
                //polyline.setFill(Color.GREEN);
               //polyline.
           if(i==3){
                polyline.setStroke(Color.WHITE);
            }
            else{
            polyline.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);     
            polyline.setStroke(Color.LIGHTGREY);
            }
            grid.getChildren().addAll(polyline);
           
            //workspace.getChildren().add(pane);
        }
        /*Polyline testpoly = new Polyline();
        Pane grid=new Pane();
        Double[] test=new Double[]{50.0, 0.0,50.0, 100.0,100.0, 0.0 , 100.0, 100.0 };
        testpoly.getPoints().addAll(test);
        grid.getChildren().add(testpoly);*/
        grid.setVisible(toggle);
        workspace.getChildren().add(grid);
    }
   

    @Override
    public void reloadWorkspace() {
        workspace.getChildren().clear();
        loadMap();
         setupHandlers();
        //initStyle();
    }

    @Override
    public void initStyle() {
        
    }
}
