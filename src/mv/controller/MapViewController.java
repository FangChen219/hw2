/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.controller;

import mv.gui.Workspace;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 *
 * @author fang
 */
public class MapViewController {
    AppTemplate app;
    AppGUI gui;
    
    
    public MapViewController(AppTemplate initApp) {
	app = initApp;
        gui=app.getGUI();
        //app.initStylesheet();
        
        
    }
    
    public void zoomIn(double x,double y){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.zoomIn(x,y);
    }
    public void zoomOut(double x,double y){
     Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.zoomOut(x,y);   
    }
    public void up(){
     Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.up();   
    }
    public void down(){
     Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.down();   
    }
    public void left(){
     Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.left();   
    }
    public void right(){
     Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.right();   
    }
    
}
