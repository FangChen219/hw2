/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import mv.data.newPoly;
import mv.data.newPolyList;
import mv.data.newSubregion;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    static final String JSON_NUMBER_OF_SUBREGIONS="NUMBER_OF_SUBREGIONS";
    static final String JSON_SUBREGIONS="SUBREGIONS";
    static final String JSON_NUMBER_OF_SUBREGION_POLYGONS="NUMBER_OF_SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGONS="SUBREGION_POLYGONS";
    
    static final String JSON_X="X";
    static final String JSON_Y="Y";
            
            
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
        dataManager.reset();
        
        JsonObject json = loadJSONFile(filePath);
        int numberOfSubregions=json.getInt(JSON_NUMBER_OF_SUBREGIONS);
        dataManager.setSize(numberOfSubregions);
        //dataManager.setNumberOfSubregions(numberOfSubregions);
        JsonArray jsonSubregionsArray= json.getJsonArray(JSON_SUBREGIONS);
        for (int i = 0; i < jsonSubregionsArray.size(); i++) {
            JsonObject jsonSubregion = jsonSubregionsArray.getJsonObject(i);
            int numberOfSubregionPolygons=jsonSubregion.getInt(JSON_NUMBER_OF_SUBREGION_POLYGONS);
            //dataManager.setNumberOfSubregionPolygons(i,numberOfSubregionPolygons);
            newSubregion subregion=LoadSubregion(jsonSubregion);
            subregion.setSize(numberOfSubregionPolygons);
            dataManager.add(subregion);
            
            /*JsonArray jsonSubregionPolyArray=jsonSubregion.getJsonArray(JSON_SUBREGION_POLYGONS);
            
            for (int j = 0; j < jsonSubregionPolyArray.size(); j++) {
                JsonObject jsonSubregionPoly=jsonSubregionPolyArray.getJsonObject(j);
                double x=getDataAsDouble(jsonSubregionPoly,JSON_X);
                double y=getDataAsDouble(jsonSubregionPoly,JSON_Y);
                newPoly poly=new newPoly(x,y);
                //dataManager.addPoly(i,j,poly);
            }*/
            
        }
        
    }
    
    public newSubregion  LoadSubregion(JsonObject jsonSubregion){
       newSubregion subregion=new newSubregion();
        JsonArray jsonSubregionPolyArray=jsonSubregion.getJsonArray(JSON_SUBREGION_POLYGONS);
            
            for (int i = 0; i < jsonSubregionPolyArray.size(); i++) {
                JsonArray jsonSubregionPoly=jsonSubregionPolyArray.getJsonArray(i);
                newPolyList polyList=new newPolyList();
                //newPoly poly=new newPoly();
               for(int j = 0; j < jsonSubregionPoly.size(); j++){
                
                JsonObject JsonPoly=jsonSubregionPoly.getJsonObject(j);
                //JsonObject JsonPolyX=jsonPoly.getJsonObject(JSON_X);
                double x=getDataAsDouble(JsonPoly,JSON_X);
                //JsonObject JsonPolyY=jsonPoly.getJsonObject(JSON_Y);
                double y=getDataAsDouble(JsonPoly,JSON_Y);
                //System.out.println("x: "+x+"y: "+y);
                //double y=getDataAsDouble(o,JSON_Y);
                newPoly poly=new newPoly(x+180,90-y);
                //poly.AddPoints(x, y);
                polyList.AddPoly(poly);
                
               }
               subregion.addPolyList(polyList);
                //newSubregion subregion = 
                //dataManager.addPoly(i,j,poly);
            }
        return subregion;
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
