/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fang
 */
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
 import javafx.scene.shape.*;
import javafx.stage.Stage;
public class test extends Application {
   
    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Welcome");
        Pane pane=new Pane();
        Polygon polygon = new Polygon();

        polygon.getPoints().addAll(new Double[]{
        0.0, 0.0,
        20.0, 10.0,
        10.0, 20.0 });
        pane.getChildren().add(polygon);
        Scene scene = new Scene(pane,300,250);
		primaryStage.setScene(scene);
		primaryStage.show();
        
    }
    public static void main(String[] args){
        launch(args);
    }
}
